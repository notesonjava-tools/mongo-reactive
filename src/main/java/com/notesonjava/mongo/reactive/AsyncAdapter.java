package com.notesonjava.mongo.reactive;

import org.bson.Document;



import com.mongodb.Block;
import com.mongodb.async.SingleResultCallback;
import com.mongodb.async.client.AggregateIterable;
import com.mongodb.async.client.FindIterable;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;

/**
 * @deprecated Prefer the Reactive Streams-based asynchronous driver (mongodb-driver-reactivestreams artifactId)
 */
@Deprecated
public class AsyncAdapter {

	public static Flowable<Document> toFlowable(FindIterable<Document> cursor) {
		
		return Flowable.create(emitter -> {
			
    		cursor
    			.forEach(new Block<Document>() {
    				@Override
    				public void apply(Document doc) {
    					emitter.onNext(doc);					
    				}
    			} , new SingleResultCallback<Void>() {
    				@Override
    				public void onResult(Void result, Throwable t) {
    					emitter.onComplete();
    				}
    			});    	    
    	}, BackpressureStrategy.BUFFER);
	}

	public static Flowable<Document> toFlowable(AggregateIterable<Document> cursor) {
		return Flowable.create(emitter -> {
			
    		cursor
    			.forEach(new Block<Document>() {
    				@Override
    				public void apply(Document doc) {
    					emitter.onNext(doc);					
    				}
    			} , new SingleResultCallback<Void>() {
    				@Override
    				public void onResult(Void result, Throwable t) {
    					emitter.onComplete();
    				}
    			});    	    
    	}, BackpressureStrategy.BUFFER);
	}
}
