# mongo-reactive

Methods to convert an async iterable from Mongo to a reactive Flowable.
Note that it doesn't look like it is possible to cancel a mongo iterable in the version 3.6.3 so, calls to Flowable.onCancel are ignored. 

Example :

	private MongoDatabase database;
	
	public Flowable<Document> findAll(){
		MongoCollection<Document> collection = database.getCollection("orders");
		FindIterable<Document> cursor = collection.find();
		return AsyncAdapter.toFlowable(cursor);
	}
